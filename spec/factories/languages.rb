# == Schema Information
#
# Table name: languages
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_languages_on_deleted_at  (deleted_at)
#  index_languages_on_slug        (slug)
#
FactoryBot.define do
  factory :language do
    name { "MyString" }
    slug { "MyString" }
    deleted_at { "2021-06-28 22:12:37" }
  end
end
