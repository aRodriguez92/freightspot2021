# == Schema Information
#
# Table name: roles
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_roles_on_deleted_at  (deleted_at)
#  index_roles_on_slug        (slug)
#
FactoryBot.define do
  factory :role do
    name { "MyString" }
    slug { "MyString" }
    deleted_at { "2021-06-28 22:08:28" }
  end
end
