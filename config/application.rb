require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Freightspot
  class Application < Rails::Application
    config.load_defaults 6.1

    config.generators do |g|
      g.assets false
      g.helper false
      g.stylesheets false
      g.template_engine :slim
    end

    config.i18n.available_locales = %i[es en]
    config.i18n.default_locale = :es
    config.i18n.load_path += Dir[Rails.root.join('config/locales/**/*.{rb,yml}')]

    config.time_zone = 'Mexico City'
  end
end
